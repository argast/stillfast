package io.stillfast.core.tapir

import java.util.UUID

import scala.annotation.unused

import pl.iterators.kebs.macros.CaseClass1Rep
//import pl.iterators.kebs.tagged.*
import sttp.tapir.Codec
import sttp.tapir.CodecFormat.TextPlain

trait TapirTaggedInstances {

  given [T, TT](using c: Codec[TT, TT, TextPlain], ev: CaseClass1Rep[T, TT]): Codec[TT, T, TextPlain] = {
    c.map(ev.apply)(ev.unapply)
  }

  given [T](using c: Codec[String, String, TextPlain], ev: CaseClass1Rep[T, UUID]): Codec[String, T, TextPlain] = {
    c.map(s => ev.apply(UUID.fromString(s)))(ev.unapply.andThen(_.toString))
  }
}
