package io.stillfast.core

import cats.data.*
import cats.implicits.*
import eu.timepit.refined.*

package object model {

  implicit class EitherOps[T](e: Either[String, T]) {
    def toValidatedNec(error: String): ValidatedNec[String, T] = e.toValidated.leftMap(_ => NonEmptyChain.one(error))
  }

}
