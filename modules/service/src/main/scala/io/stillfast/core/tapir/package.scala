package io.stillfast.core

// import io.scalaland.chimney.*
//import io.scalaland.chimney.dsl.*
import sttp.tapir.*
import sttp.tapir.json.zio.TapirJsonZio
import sttp.tapir.server.vertx.zio.VertxZioServerInterpreter
import sttp.tapir.ztapir.ZTapir
import zio.Clock
import zio.ZIO

import io.stillfast.core.error.*
import io.stillfast.core.transformer.*
import io.stillfast.core.transformer.syntax.*

package object tapir extends VertxZioServerInterpreter[Clock] with TapirJsonZio with Tapir with ZTapir {

  type Endpoint[I, E, O, -R] = sttp.tapir.PublicEndpoint[I, E, O, R]

  implicit class EndpointTransformOps[I, E >: AppError, O, -R](e: Endpoint[I, E, O, R]) {
    def zTransformServerLogic[R2, I2, O2](
        logic: I2 => ZIO[R2, E, O2],
    )(implicit it: ValidatedTransformer[I, I2], ot: Transformer[O2, O]) =
      e.zServerLogic { i =>
        ZIO.fromEither(i.validateInto[I2]).flatMap(logic.andThen(_.map(_.into[O])))
      }
  }
}
