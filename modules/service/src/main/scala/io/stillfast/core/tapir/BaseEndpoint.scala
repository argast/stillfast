package io.stillfast.core.tapir

import sttp.model.StatusCode
import sttp.tapir.generic.auto.*

import io.stillfast.core.error.*

trait BaseEndpoint extends TapirTaggedInstances {

  def baseEndpoint = endpoint
    .errorOut(
      oneOf[AppError](
        oneOfVariantClassMatcher(
          StatusCode.BadRequest,
          jsonBody[AppError].description("Validation error"),
          classOf[ValidationError],
        ),
        oneOfVariantClassMatcher(
          StatusCode.NotFound,
          jsonBody[AppError].description("Resource not found"),
          classOf[ResourceNotFound],
        ),
        oneOfVariantClassMatcher(
          StatusCode.InternalServerError,
          jsonBody[AppError].description("Server error"),
          classOf[AppError],
        ),
        oneOfDefaultVariant(jsonBody[AppError].description("Unknown error")),
      ),
    )
}
