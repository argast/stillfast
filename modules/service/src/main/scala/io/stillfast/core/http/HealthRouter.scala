package io.stillfast.core.http

import sttp.tapir.PublicEndpoint
import zio.*

import io.stillfast.core.error.*
import io.stillfast.core.tapir.BaseEndpoint
import io.stillfast.core.tapir.*

object HealthRouter extends BaseEndpoint {

  object Endpoints {
    def health: PublicEndpoint[Unit, AppError, String, Any] =
      baseEndpoint.get
        .in("health")
        .out(stringBody)
  }

  def healthRoute: ZServerEndpoint[Any, Any] =
    Endpoints.health.zServerLogic(_ => ZIO.succeed("OK"))
}
