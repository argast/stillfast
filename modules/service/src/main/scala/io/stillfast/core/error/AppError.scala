package io.stillfast.core.error

import cats.data.NonEmptyChain
import sttp.tapir.Schema
import zio.json.*

abstract class AppError {
  val message: String
}
object AppError {
  private case class AppErrorResponse(message: String)

  private given JsonCodec[AppErrorResponse] = DeriveJsonCodec.gen

  given JsonCodec[AppError] = given_JsonCodec_AppErrorResponse.transform(
    e =>
      new AppError {
        override val message: String = e.message
      },
    e => AppErrorResponse(e.message),
  )

  given (using s: Schema[String]): Schema[AppError] = s.as[AppError]
}

abstract class ValidationError extends AppError
abstract class ResourceNotFound extends AppError

case class ResourceValidationError private[error] (errors: List[String]) extends ValidationError {
  override val message: String = ""
}
object ResourceValidationError {
  def apply(errors: NonEmptyChain[String]): ResourceValidationError =
    ResourceValidationError(errors.toNonEmptyList.toList)
}

case class UnknownError private[error] (message: String) extends AppError
object UnknownError {
  def apply(): UnknownError = UnknownError("Unknown error")
}
