//package io.stillfast.core
//
//import scala.collection.compat.Factory
//
//// import io.scalaland.chimney.*
////import io.scalaland.chimney.dsl.*
//import zio.*
//
//import io.stillfast.core.error.ResourceValidationError
//
//package object chimney {
//
//  type TF[+T] = IO[List[String], T]
//
//  implicit def transformerTFSupport: TransformerFSupport[TF] = new TransformerFSupport[TF] {
//    override def pure[A](value: A): TF[A] = ZIO.succeed(value)
//    override def product[A, B](fa: TF[A], fb: => TF[B]): TF[(A, B)] = fa.zip(fb)
//    override def map[A, B](fa: TF[A], f: A => B): TF[B] = fa.map(f)
//    override def traverse[M, A, B](it: Iterator[A], f: A => TF[B])(implicit fac: Factory[B, M]): TF[M] = {
//      ZIO
//        .foldLeft(it.toSeq)((Seq.empty[String], Seq.empty[B])) { case ((failures, successes), a) =>
//          f(a).foldZIO(
//            errors => ZIO.succeed((failures ++ errors, successes)),
//            success => ZIO.succeed((failures, successes :+ success)),
//          )
//        }
//        .flatMap { case (failures, successes) =>
//          ZIO.cond(failures.isEmpty, fac.newBuilder.addAll(successes).result(), failures.toList)
//        }
//    }
//  }
//
//  implicit class zTransform[From](v: From) {
//    def zTransformInto[To](implicit t: TransformerF[TF, From, To]): ZIO[Any, ResourceValidationError, To] =
//      v.transformIntoF[IO[List[String], +*], To].mapError(ResourceValidationError(_))
//  }
////
////  implicit def transformer[From, To](implicit t: Transformer[From, To]): TransformerF[TF, From, To] =
////    new TransformerF[TF, From, To] {
////      override def transform(src: From): TF[To] = src.transformInto[To].validNec[String]
////    }
//
//}
