package io.stillfast.core.transformer

import cats.data.Validated
import io.stillfast.core.error.AppError

trait Transformer[From, To] {
  def transform(from: From): To
}

trait ValidatedTransformer[From, To] {
  def transform(from: From): Either[AppError, To]
}
