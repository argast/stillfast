package io.stillfast.core

import zio.*

import io.stillfast.core.tapir.ZServerEndpoint

trait ModuleCompanion {
  type Env
  type InternalEnv
  type DependenciesEnv
}

trait HttpModule[T <: ModuleCompanion](val module: T) {
  def routes: List[ZServerEndpoint[module.InternalEnv & module.DependenciesEnv, Any]]
}
