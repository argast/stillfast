package io.stillfast.core.transformer

import cats.data.Validated
import io.stillfast.core.error.AppError

object syntax {

  implicit class TransformerSyntax[From](from: From) {
    def into[To](using t: Transformer[From, To]): To = t.transform(from)
    def validateInto[To](using t: ValidatedTransformer[From, To]): Either[AppError, To] = t.transform(from)
  }
}
