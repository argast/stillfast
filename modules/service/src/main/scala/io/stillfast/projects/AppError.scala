package io.stillfast.projects

import java.util.UUID
import io.stillfast.core.error.ResourceNotFound
import io.stillfast.projects.model.Project

object AppError {

  case class ProjectNotFound(id: Project.Id) extends ResourceNotFound {
    override val message: String = s"Project $id not found"
  }
}
