package io.stillfast.projects

import zio.*

import io.stillfast.core.*
import io.stillfast.core.ModuleCompanion
import io.stillfast.core.error.*
import io.stillfast.core.tapir.*
import io.stillfast.projects.http.router.ProjectRouter
import io.stillfast.projects.model.Project
import io.stillfast.projects.repository.ProjectRepository

abstract class ProjectsModule extends HttpModule(ProjectsModule) {
  def getProject(id: Project.Id): ZIO[ProjectRepository, AppError, Option[Project]]
}

object ProjectsModule extends ModuleCompanion {

  override type DependenciesEnv = Any
  override type InternalEnv = ProjectRepository
  override type Env = ProjectsModule & InternalEnv

  object LiveProjectsModule extends ProjectsModule {
    val routes: List[ZServerEndpoint[InternalEnv & DependenciesEnv, Any]] = ProjectRouter.routes

    override def getProject(id: Project.Id): ZIO[ProjectRepository, AppError, Option[Project]] =
      ZIO.serviceWithZIO[ProjectRepository](_.getProject(id))

    val layer: ZLayer[InternalEnv, Nothing, ProjectsModule] =
      ZLayer.succeed(LiveProjectsModule)
  }

  def getProject(id: Project.Id): ZIO[Env, AppError, Option[Project]] =
    ZIO.serviceWithZIO[ProjectsModule](_.getProject(id))

  val layer: ZLayer[Any, Nothing, ProjectsModule.Env] =
    ProjectRepository.inMemory >+> LiveProjectsModule.layer
}
