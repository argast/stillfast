package io.stillfast.projects.http.router

//import io.scalaland.chimney.dsl.*
import sttp.tapir.generic.auto.*
import io.stillfast.core.error.*
import io.stillfast.core.tapir.BaseEndpoint
import io.stillfast.core.tapir.*
import io.stillfast.projects.AppError.ProjectNotFound
import io.stillfast.projects.http.resources.CreateProjectRequest
import io.stillfast.projects.http.resources.ProjectResponse
import io.stillfast.projects.model.Project
import io.stillfast.projects.repository.ProjectRepository

import java.util.UUID

object ProjectRouter extends BaseEndpoint {

  object Endpoints {
    def createProject: Endpoint[CreateProjectRequest, AppError, ProjectResponse, Any] =
      baseEndpoint.post
        .in("projects")
        .in(jsonBody[CreateProjectRequest])
        .out(jsonBody[ProjectResponse])
    def getProject: Endpoint[Project.Id, AppError, ProjectResponse, Any] =
      baseEndpoint.get
        .in("projects")
        .in(path[Project.Id])
        .out(jsonBody[ProjectResponse])
//    def getProjects: Endpoint[Unit, ServiceError, List[ProjectResponse], Any] =
//      baseEndpoint.get
//        .in("projects")
//        .out(jsonBody[List[ProjectResponse]])

  }

  def createProjectRoute: ZServerEndpoint[ProjectRepository, Any] =
    Endpoints.createProject.zTransformServerLogic { (project: Project) =>
      for {
        _ <- ProjectRepository.insertProject(project)
        result <- ProjectRepository.getProject(project.id).someOrFail(UnknownError())
      } yield result
    }

  def getProjectRoute: ZServerEndpoint[ProjectRepository, Any] =
    Endpoints.getProject
      .zServerLogic { (projectId: Project.Id) =>
        for {
          result <- ProjectRepository.getProject(projectId).someOrFail(ProjectNotFound(projectId))
        } yield ProjectResponse.from(result)
      }
//
//  def getProjectsRoute: ZServerEndpoint[ProjectRepository, Any] = Endpoints.getProjects
//    .zServerLogic { _ =>
//      for {
//        result <- ProjectRepository.getProjects()
//      } yield result.map(_.transformInto[ProjectResponse])
//    }

  def routes: List[ZServerEndpoint[ProjectRepository, Any]] = List(
    createProjectRoute,
    getProjectRoute,
//    getProjectsRoute,
  )
}
