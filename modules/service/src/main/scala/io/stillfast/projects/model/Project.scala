package io.stillfast.projects.model

import io.stillfast.core.transformer.*

import java.util.UUID

import pl.iterators.kebs.opaque.*

import io.stillfast.projects.http.resources.CreateProjectRequest
import io.stillfast.projects.http.resources.ProjectResponse

case class Project(id: Project.Id, name: Project.Name)

object Project {

  opaque type Id = UUID
  object Id extends Opaque[Id, UUID]

  opaque type Name = String
  object Name extends Opaque[Name, String]

  implicit val fromCreateProjectRequestTransformer: ValidatedTransformer[CreateProjectRequest, Project] = from =>
    Right(
      Project(
        id = Project.Id(UUID.randomUUID()),
        name = from.name,
      ),
    )

  implicit val toProjectResponseTransformer: Transformer[Project, ProjectResponse] = from =>
    ProjectResponse(from.id, from.name)
}
