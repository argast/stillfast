package io.stillfast.projects.http.resources

import io.stillfast.projects.model.Project

import java.util.UUID
import zio.json.*

case class ProjectResponse(id: UUID, name: String)
object ProjectResponse {
  given JsonCodec[ProjectResponse] = DeriveJsonCodec.gen

  def from(project: Project) = ProjectResponse(project.id.unwrap, project.name.unwrap)
}
