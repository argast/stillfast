package io.stillfast.projects.http.resources

import zio.json.*

case class CreateProjectRequest(name: String)

object CreateProjectRequest {
  given JsonCodec[CreateProjectRequest] = DeriveJsonCodec.gen
}
