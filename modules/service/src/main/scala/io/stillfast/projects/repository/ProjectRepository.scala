package io.stillfast.projects.repository

import zio.*

import io.stillfast.core.error.AppError
import io.stillfast.projects.model.Project

trait ProjectRepository {
  def insertProject(project: Project): ZIO[Any, AppError, Project]
  def getProject(id: Project.Id): ZIO[Any, AppError, Option[Project]]
}

object ProjectRepository {

  object InMemoryProjectRepository extends ProjectRepository {
    private val projects = scala.collection.mutable.Map.empty[Project.Id, Project]

    override def getProject(id: Project.Id): ZIO[Any, AppError, Option[Project]] = ZIO.succeed(projects.get(id))

    override def insertProject(project: Project): ZIO[Any, AppError, Project] = ZIO.succeed {
      projects += (project.id -> project)
      project
    }
  }

  def getProject(id: Project.Id): ZIO[ProjectRepository, AppError, Option[Project]] =
    ZIO.serviceWithZIO[ProjectRepository](_.getProject(id))

  def insertProject(project: Project): ZIO[ProjectRepository, AppError, Project] =
    ZIO.serviceWithZIO[ProjectRepository](_.insertProject(project))

  val inMemory: ZLayer[Any, Nothing, ProjectRepository] = ZLayer.succeed(InMemoryProjectRepository)
}
