package io.stillfast

import zio.*
import zio.logging.LogFormat
import zio.logging.backend.SLF4J

import io.stillfast.config.AppConfig

object App extends zio.ZIOAppDefault {

  type Env = AppConfig & DatabaseMigrator & Server & Scope & simulations.SimulationsModule.Env &
    projects.ProjectsModule.Env

  private val logger = {
    Runtime.removeDefaultLoggers >>> SLF4J.slf4j(
      LogFormat.line |-| LogFormat.cause,
    )
  }

  val app: ZIO[Env, Throwable, Unit] = {
    (for {
      _ <- DatabaseMigrator.migrate()
      _ <- ZIO.acquireRelease(Server.start())(_ => Server.stop())
    } yield ()) *> ZIO.never
  }

  override def run = {
    app.provide(
      projects.ProjectsModule.layer,
      simulations.SimulationsModule.layer,
      AppConfig.layer,
      DatabaseMigrator.layer,
      Server.layer,
      Scope.default,
      logger,
    )
  }
}
