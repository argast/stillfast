package io.stillfast.simulations.http

//import io.scalaland.chimney.dsl.*
import sttp.tapir.generic.auto.*
import zio.*

import java.util.UUID

//import io.stillfast.core.chimney.*
import io.scalaland.chimney.syntax.*
import io.stillfast.core.error.*
import io.stillfast.core.transformer.syntax.*
import io.stillfast.core.tapir.BaseEndpoint
import io.stillfast.core.tapir.ZServerEndpoint
import io.stillfast.core.tapir.*
import io.stillfast.projects.AppError.ProjectNotFound
import io.stillfast.projects.ProjectsModule
import io.stillfast.projects.model.Project
import io.stillfast.simulations.SimulationsModule
import io.stillfast.simulations.http.resources.NewPerformanceTestRequest
import io.stillfast.simulations.http.resources.PerformanceTestResponse
import io.stillfast.simulations.model.PerformanceTest
import io.stillfast.simulations.repository.GatlingSimulationRepository

object PerformanceTestRouter extends BaseEndpoint {

  object Endpoints {
    def createSimulation: Endpoint[(Project.Id, NewPerformanceTestRequest), AppError, PerformanceTestResponse, Any] =
      baseEndpoint.post
        .in("projects")
        .in(path[Project.Id])
        .in("tests")
        .in(jsonBody[NewPerformanceTestRequest])
        .out(jsonBody[PerformanceTestResponse])
    def getSimulation: Endpoint[(Project.Id, PerformanceTest.Id), AppError, PerformanceTestResponse, Any] =
      baseEndpoint.get
        .in("projects")
        .in(path[Project.Id])
        .in("tests")
        .in(path[PerformanceTest.Id])
        .out(jsonBody[PerformanceTestResponse])
    def getSimulations: Endpoint[Project.Id, AppError, List[PerformanceTestResponse], Any] =
      baseEndpoint.get
        .in("projects")
        .in(path[Project.Id])
        .in("tests")
        .out(jsonBody[List[PerformanceTestResponse]])
  }

  def createRoute: ZServerEndpoint[SimulationsModule.InternalEnv & ProjectsModule.Env, Any] =
    Endpoints.createSimulation
      .zServerLogic { case (projectId, request) =>
        val performanceTest = request.transformInto[PerformanceTest]
        for {
          _ <- ProjectsModule.getProject(projectId).someOrFail(ProjectNotFound(projectId))
          _ <- GatlingSimulationRepository.insertSimulation(performanceTest)
          result <- GatlingSimulationRepository.getSimulation(performanceTest.id).someOrFail(UnknownError())
        } yield result.into[PerformanceTestResponse]
      }

  def getSimulationRoute: ZServerEndpoint[SimulationsModule.InternalEnv & ProjectsModule.Env, Any] =
    Endpoints.getSimulation
      .zServerLogic { case (projectId, testId) =>
        for {
          _ <- ProjectsModule.getProject(projectId).someOrFail(ProjectNotFound(projectId))
          result <- GatlingSimulationRepository.getSimulation(testId).someOrFail(UnknownError())
        } yield result.into[PerformanceTestResponse]
      }

  def getSimulationsRoute: ZServerEndpoint[SimulationsModule.InternalEnv & ProjectsModule.Env, Any] =
    Endpoints.getSimulations
      .zServerLogic { projectId =>
        for {
          results <- GatlingSimulationRepository.getSimulations()
        } yield results.map(_.into[PerformanceTestResponse])
      }

  def routes: List[ZServerEndpoint[SimulationsModule.InternalEnv & ProjectsModule.Env, Any]] = List(
    createRoute,
    getSimulationRoute,
    getSimulationsRoute,
  )
}
