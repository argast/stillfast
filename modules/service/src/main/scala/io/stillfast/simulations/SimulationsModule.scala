package io.stillfast.simulations

import zio.*

import io.stillfast.core.*
import io.stillfast.core.ModuleCompanion
import io.stillfast.core.tapir.*
import io.stillfast.projects.ProjectsModule
import io.stillfast.simulations.http.PerformanceTestRouter
import io.stillfast.simulations.repository.GatlingSimulationRepository

abstract class SimulationsModule extends HttpModule(SimulationsModule)

object SimulationsModule extends ModuleCompanion {

  override type DependenciesEnv = ProjectsModule.Env
  override type InternalEnv = GatlingSimulationRepository
  override type Env = SimulationsModule & InternalEnv

  object LiveSimulationsModule extends SimulationsModule {
    val routes: List[ZServerEndpoint[InternalEnv & DependenciesEnv, Any]] = PerformanceTestRouter.routes

    val layer: ZLayer[InternalEnv, Nothing, SimulationsModule] =
      ProjectsModule.layer >+> ZLayer.succeed(LiveSimulationsModule)

  }

  val layer: ZLayer[Any, Nothing, SimulationsModule.Env] =
    GatlingSimulationRepository.inMemory >+> LiveSimulationsModule.layer
}
