package io.stillfast.simulations.http.resources

import java.time.LocalDateTime
import java.util.UUID
import eu.timepit.refined.types.string.FiniteString
import io.stillfast.core.transformer.Transformer
//import io.scalaland.chimney.Transformer
//import scalaz.annotation.deriving
import zio.json.*
//import zio.json.interop.refined.*

//import io.stillfast.core.chimney.ChimneyTaggedInstances
import io.stillfast.core.model.*
import io.stillfast.simulations.model.PerformanceTest

case class PerformanceTestResponse(
    id: UUID,
    name: String,
    startedAt: LocalDateTime,
    endedAt: LocalDateTime,
)
object PerformanceTestResponse {
  given JsonCodec[PerformanceTestResponse] = DeriveJsonCodec.gen

  implicit val transformer: Transformer[PerformanceTest, PerformanceTestResponse] = from =>
    PerformanceTestResponse(
      id = from.id.unwrap,
      name = from.name.unwrap,
      startedAt = from.startedAt.unwrap,
      endedAt = from.endedAt.unwrap,
    )
}
