package io.stillfast.simulations.repository

import zio.*

import io.stillfast.core.error.*
import io.stillfast.simulations.model.*

trait GatlingSimulationRepository {
  def addSimulation(performanceTest: PerformanceTest): ZIO[Any, AppError, Unit]
  def getSimulation(id: PerformanceTest.Id): ZIO[Any, AppError, Option[PerformanceTest]]
  def getSimulations(): ZIO[Any, AppError, List[PerformanceTest]]
}

object GatlingSimulationRepository {
  def insertSimulation(performanceTest: PerformanceTest): ZIO[GatlingSimulationRepository, AppError, Unit] =
    ZIO.serviceWithZIO[GatlingSimulationRepository](_.addSimulation(performanceTest))

  def getSimulation(id: PerformanceTest.Id): ZIO[GatlingSimulationRepository, AppError, Option[PerformanceTest]] =
    ZIO.serviceWithZIO[GatlingSimulationRepository](_.getSimulation(id))

  def getSimulations(): ZIO[GatlingSimulationRepository, AppError, List[PerformanceTest]] =
    ZIO.serviceWithZIO[GatlingSimulationRepository](_.getSimulations())

  def inMemory: ZLayer[Any, Nothing, GatlingSimulationRepository] =
    ZLayer.succeed(InMemoryGatlingSimulationRepository())
}

case class InMemoryGatlingSimulationRepository() extends GatlingSimulationRepository {
  private val simulations = scala.collection.mutable.Map.empty[PerformanceTest.Id, PerformanceTest]

  override def addSimulation(performanceTest: PerformanceTest): ZIO[Any, AppError, Unit] = {
    ZIO.succeed(simulations += (performanceTest.id -> performanceTest))
  }

  override def getSimulation(id: PerformanceTest.Id): ZIO[Any, AppError, Option[PerformanceTest]] = ZIO.succeed {
    simulations.get(id)
  }

  override def getSimulations(): ZIO[Any, AppError, List[PerformanceTest]] = ZIO.succeed {
    simulations.values.toList
  }
}

object InMemoryGatlingSimulationRepository {}

//case class LiveGatlingSimulationRepository() extends GatlingSimulationRepository {
//
//  implicit val jsonMeta: Meta[Json] =
//    Meta.Advanced
//      .other[PGobject]("json")
//      .timap[Json](
//        _.getValue
//          .fromJson[Json]
//          .fold(
//            e => throw new Exception(e),
//            identity,
//          ),
//      )(json => {
//        val o = new PGobject
//        o.setType("json")
//        o.setValue(json.toJson)
//        o
//      })
//
//  def insertSimulation(s: PerformanceTest): IO[ServiceError, Unit] = {
//    for {
//      json <- toJsonAst(s)
//      _ <- sql"""
//            INSERT INTO gatling_simulations (id, data)
//            VALUES (${s.id}, $json)
//         """.update.run
//        .transact(xa)
//        .orDie
//    } yield ()
//  }
//
//  private def toJsonAst[T: JsonEncoder](v: T) = ZIO.fromEither(v.toJsonAST).leftMap(_ => UnknownError())
//
//  def zparse[T: JsonDecoder](json: Json): IO[ServiceError, T] =
//    ZIO.fromEither(JsonDecoder[T].fromJsonAST(json)).leftMap(_ => UnknownError())
//
//  def getSimulation(id: UUID): IO[ServiceError, Option[PerformanceTest]] = {
//    for {
//      json <- sql"""SELECT data FROM gatling_simulations WHERE id = $id"""
//        .query[Json]
//        .option
//        .transact(xa)
//        .orDie
//      result <- json.traverse(zparse[PerformanceTest])
//    } yield result
//  }
//
//  def getSimulations: IO[ServiceError, List[PerformanceTest]] = {
//    for {
//      json <- sql"""SELECT data FROM gatling_simulations"""
//        .query[Json]
//        .to[List]
//        .transact(xa)
//        .orDie
//      result <- json.traverse(zparse[PerformanceTest])
//    } yield result
//  }
//}
