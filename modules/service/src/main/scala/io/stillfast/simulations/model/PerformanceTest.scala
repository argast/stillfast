package io.stillfast.simulations.model

import java.time.LocalDateTime
import java.util.UUID
import eu.timepit.refined.types.string.FiniteString
import io.scalaland.chimney.*
import io.scalaland.chimney.syntax.*
// import io.scalaland.chimney.cats.*
import pl.iterators.kebs.opaque.*

//import io.stillfast.core.chimney.ChimneyTaggedInstances
//import io.stillfast.core.chimney.*
import io.stillfast.core.model.*
import io.stillfast.simulations.http.resources.NewPerformanceTestRequest

case class PerformanceTest(
    id: PerformanceTest.Id,
    name: PerformanceTest.Name,
    startedAt: PerformanceTest.StartedAt,
    endedAt: PerformanceTest.EndedAt,
)

object PerformanceTest {

  opaque type Id = UUID
  object Id extends Opaque[Id, UUID]

  opaque type Name = String
  object Name extends Opaque[Name, String]

  opaque type StartedAt = LocalDateTime
  object StartedAt extends Opaque[StartedAt, LocalDateTime]

  opaque type EndedAt = LocalDateTime
  object EndedAt extends Opaque[EndedAt, LocalDateTime]

  implicit val transformer: Transformer[NewPerformanceTestRequest, PerformanceTest] =
    Transformer
      .define[NewPerformanceTestRequest, PerformanceTest]
      .withFieldConst(_.id, PerformanceTest.Id(UUID.randomUUID()))
      .buildTransformer

}
