package io.stillfast.simulations.http.resources

import java.time.LocalDateTime

import eu.timepit.refined.types.string.FiniteString
//import scalaz.annotation.deriving
import zio.json.*
//import zio.json.interop.refined.*

import io.stillfast.core.model.*

case class NewPerformanceTestRequest(
    name: String,
    startedAt: LocalDateTime,
    endedAt: LocalDateTime,
)
object NewPerformanceTestRequest {
  given JsonCodec[NewPerformanceTestRequest] = DeriveJsonCodec.gen
}
