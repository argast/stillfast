package io.stillfast

import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import sttp.tapir.server.vertx.zio.VertxZioServerInterpreter
import sttp.tapir.server.vertx.zio.VertxZioServerInterpreter.*
import sttp.tapir.ztapir.*
import zio.*

import io.stillfast.App.Env
import io.stillfast.config.*
import io.stillfast.core.http.HealthRouter
import io.stillfast.projects.ProjectsModule
import io.stillfast.simulations.SimulationsModule

trait Server {
  def start(): ZIO[App.Env, Throwable, Unit]
  def stop(): ZIO[Scope, Nothing, Unit]
}

object Server {

  case class TapirServer(config: AppConfig) extends Server {

    lazy val vertx = Vertx.vertx()
    lazy val server = vertx.createHttpServer()
    lazy val router = Router.router(vertx)

    def start(): ZIO[App.Env, Throwable, Unit] = {
      ZIO.runtime[App.Env].zip(ZIO.service[SimulationsModule]).zip(ZIO.service[ProjectsModule]).flatMap {
        case (runtime, simulationsModule, projectsModule) =>
          val routes = List(
            HealthRouter.healthRoute.widen[App.Env],
          ) ++ simulationsModule.routes.map(_.widen[App.Env]) ++ projectsModule.routes.map(_.widen[App.Env])
          handleRoutes(routes)(runtime)
      }
    }

    private def handleRoutes(routes: List[ZServerEndpoint[App.Env, Any]])(implicit runtime: Runtime[App.Env]) = {
      val interpreter = VertxZioServerInterpreter[App.Env]()
      routes.foreach(interpreter.route(_)(runtime).apply(router))
      ZIO
        .attempt {
          server
            .requestHandler(router)
            .listen(config.port, config.interface)
        }
        .flatMap(_.asRIO)
        .unit
    }

    def stop(): ZIO[Scope, Nothing, Unit] = ZIO.attempt(server.close()).flatMap(_.asRIO).unit.orDie

  }

  def start(): ZIO[Env with Server, Throwable, Unit] = ZIO.serviceWithZIO[Server](_.start())
  def stop(): ZIO[Scope with Server, Nothing, Unit] = ZIO.serviceWithZIO[Server](_.stop())

  def layer: ZLayer[AppConfig, Throwable, Server] = ZLayer {
    ZIO.service[AppConfig].map(TapirServer(_))
  }
}
