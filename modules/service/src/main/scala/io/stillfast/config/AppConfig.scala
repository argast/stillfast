package io.stillfast.config

import pureconfig.*
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.derivation.default.*
import zio.*

case class DatabaseConfig(url: String, username: String, password: String) derives ConfigReader

case class AppConfig(interface: String, port: Int, database: DatabaseConfig) derives ConfigReader
object AppConfig {
  val layer: ZLayer[Any, ConfigReaderFailures, AppConfig] =
    ZLayer.fromZIO(ZIO.fromEither(ConfigSource.default.load[AppConfig]))
}
