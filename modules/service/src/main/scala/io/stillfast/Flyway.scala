package io.stillfast

import zio.*

import io.stillfast.config.AppConfig
import io.stillfast.config.DatabaseConfig

trait DatabaseMigrator {
  def migrate(): ZIO[DatabaseMigrator & Scope, Throwable, Unit]
}

object DatabaseMigrator {

  case class FlywayDatabaseMigrator(databaseConfig: DatabaseConfig) extends DatabaseMigrator {
    def migrate(): ZIO[DatabaseMigrator & Scope, Throwable, Unit] = {

//      val flyway = org.flywaydb.core.Flyway.configure
//        .dataSource(
//          databaseConfig.url,
//          databaseConfig.username,
//          databaseConfig.password,
//        )
//        .load()
      for {
        _ <- ZIO.logInfo("Migrating database")
        _ <- ZIO.succeed(())
      } yield ()
    }

    //    ZIO
    //      .attemptBlocking(flyway.migrate())
    //      .tap(_ => logger.infoIO("Migrated"))
    //      .tapError(_ => logger.infoIO("Could not migrate database. Retrying..."))
    //      .retry(Schedule.recurs(10).addDelay(_ => 1 second))
    //      .tapError(err => logger.infoIO("Database migration failed: " + err.getMessage))
  }

  def migrate(): ZIO[DatabaseMigrator & Scope, Throwable, Unit] = ZIO.serviceWithZIO[DatabaseMigrator](_.migrate())

  def layer: ZLayer[AppConfig, Nothing, DatabaseMigrator] = ZLayer.fromZIO {
    ZIO.service[AppConfig].map(serviceConfig => FlywayDatabaseMigrator(serviceConfig.database))
  }

}
