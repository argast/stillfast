--CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE gatling_simulations (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    data JSONB NOT NULL
);