package io.stillfast

import org.scalactic.Prettifier
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import zio.*

trait BaseTest extends AnyWordSpec with Matchers with RandomSupport {

  protected lazy val runtime = Runtime.default

  protected given Prettifier = Prettifier {
    case any => {
      pprint.apply(any).render
    }
  }

  implicit class ZOps[E, A](zio: ZIO[Any, E, A])(implicit u: Unsafe) {
    def zValue: A = runtime.unsafe.run(zio).getOrThrowFiberFailure()
  }
}
