//package io.stillfast
//
//import cats.effect
//import cats.effect.Blocker
//import com.dimafeng.testcontainers.*
//import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
//import doobie.hikari.HikariTransactor
//import doobie.implicits.*
//import doobie.util.ExecutionContexts
//import io.stillfast.config.DatabaseConfig
//import org.scalatest.BeforeAndAfterEach
//import org.testcontainers.utility.DockerImageName
//import zio.{Task, ZIO}
//import zio.interop.catz.*
//import zio.interop.catz.implicits.*
//
//import java.util.concurrent.Executors
//import scala.concurrent.ExecutionContext
//
//abstract class BaseRepositoryTest[Repository] extends BaseTest with BeforeAndAfterEach {
//
//  implicit val transactor = TestDatabase.transactor
//
//  override protected def beforeEach(): Unit = {
//    super.beforeEach()
//    TestDatabase.truncate()
//  }
//}
