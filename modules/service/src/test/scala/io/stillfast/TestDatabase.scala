//package io.stillfast
//
//import cats.effect.Blocker
//import com.dimafeng.testcontainers.PostgreSQLContainer
//import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
//import doobie.hikari.HikariTransactor
//import doobie.implicits.*
//import io.stillfast.config.DatabaseConfig
//import org.testcontainers.utility.DockerImageName
//import zio.*
//import zio.interop.catz.*
//import zio.interop.catz.implicits.*
//
//import java.util.concurrent.Executors
//import scala.concurrent.ExecutionContext
//
//object TestDatabase {
//
//  private val container = PostgreSQLContainer(DockerImageName.parse("postgres:13.3-alpine"))
//  private val runtime = Runtime.default
//
//  implicit lazy val transactor: HikariTransactor[Task] = {
//    container.start()
//    val config = DatabaseConfig(container.jdbcUrl, container.username, container.password)
//    runtime.unsafeRun(Flyway.migrate(config).unit)
//    createTransactor()
//  }
//
//  private def createTransactor() = {
//    val config = new HikariConfig()
//    config.setJdbcUrl(container.jdbcUrl)
//    config.setUsername(container.username)
//    config.setPassword(container.password)
//    val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(16))
//    HikariTransactor[Task](
//      new HikariDataSource(config),
//      ec,
//      Blocker.liftExecutionContext(ec),
//    )
//  }
//
//  def truncate() = {
//    runtime.unsafeRun {
//      sql"""
//      TRUNCATE gatling_simulations CASCADE
//    """.update.run.transact(transactor).unit
//    }
//  }
//}
