//package io.stillfast.simulations.repository
//
//import doobie.hikari.HikariTransactor
//import doobie.implicits.*
//import eu.timepit.refined.auto.*
//import io.stillfast.BaseRepositoryTest
//import io.stillfast.simulations.model.PerformanceTest
//import magnolify.scalacheck.auto.*
//import org.scalatest.OptionValues
//import zio.*
//import io.stillfast.arbitrary.all.*
//import eu.timepit.refined.scalacheck.all.*
//
//class GatlingRepositoryTest extends BaseRepositoryTest[GatlingSimulationRepository] with OptionValues {
//
//  private lazy val repository = new GatlingSimulationRepository()
//
//  "GatlingSimulationRepository" should {
//    "create simulation" in {
//      val newSimulation = random[PerformanceTest].copy(name = "test")
//      repository.insertSimulation(newSimulation).zValue
//      val result = repository.getSimulation(newSimulation.id).zValue
//      result.value.name.value should ===("test")
//      result.value should ===(newSimulation)
//    }
//
//    "get simulations" in {
//      val newSimulation1 = random[PerformanceTest]
//      val newSimulation2 = random[PerformanceTest]
//      repository.insertSimulation(newSimulation1).zValue
//      repository.insertSimulation(newSimulation2).zValue
//      val result = repository.getSimulations.zValue
//      result should contain theSameElementsAs (Seq(newSimulation1, newSimulation2))
//    }
//  }
//}
