package io.stillfast

import org.scalacheck.Arbitrary

trait RandomSupport {

  def random[T: Arbitrary] = Arbitrary.arbitrary[T].sample.get
}
