package io.stillfast.arbitrary

import java.time.LocalDateTime
import java.time.ZoneOffset

import org.scalacheck.Arbitrary
import org.scalacheck.Gen.*

trait JavaTimeInstances {

  implicit lazy val arbLocalDateTime: Arbitrary[LocalDateTime] = {
    import ZoneOffset.UTC
    Arbitrary {
      for {
        seconds <- chooseNum(
          LocalDateTime.of(1970, 1, 1, 0, 0).toEpochSecond(UTC),
          LocalDateTime.of(2099, 1, 1, 0, 0).toEpochSecond(UTC),
        )
        nanos <- chooseNum(LocalDateTime.MIN.getNano, LocalDateTime.MAX.getNano)
      } yield LocalDateTime.ofEpochSecond(seconds, nanos, UTC)
    }
  }

}
