package io.stillfast

import io.gatling.core.Predef.*
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef.*

import scala.concurrent.duration.*
import scala.language.postfixOps

class SmokeSimulation extends Simulation {

  val httpConf = http.baseUrl(s"http://localhost:8080")

  val scn = scenario("Health Simulation").during(10 seconds) {
    exec(
      http("health").get("/health"),
    )
  }

  setUp(scn.inject(atOnceUsers(10))).protocols(httpConf)
}
