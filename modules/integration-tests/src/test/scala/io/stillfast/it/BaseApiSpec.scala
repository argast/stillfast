package io.stillfast.it

import pureconfig.*
import pureconfig.error.ConfigReaderFailures
import sttp.client3.httpclient.zio.HttpClientZioBackend
import zio.*
import zio.logging.LogFormat
import zio.logging.backend.SLF4J
import zio.test.ZIOSpec

import io.stillfast.config.AppConfig

abstract class BaseApiSpec extends ZIOSpec[TestApiClient] {

  private val logger = {
    Runtime.removeDefaultLoggers >>> SLF4J.slf4j(
      LogFormat.line |-| LogFormat.cause,
    )
  }

  private lazy val backend = HttpClientZioBackend.layer()

  private lazy val stack = StillFastContainer.layer()

  override def bootstrap: ZLayer[Any, Any, TestApiClient] = {
    logger >+> stack >+> backend >+> TestApiClient.layer
  }
}
