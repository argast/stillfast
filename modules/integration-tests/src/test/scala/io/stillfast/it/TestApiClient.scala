package io.stillfast.it

import com.dimafeng.testcontainers.GenericContainer
import sttp.client3.Request
import sttp.client3.SttpBackend
import sttp.client3.*
import sttp.tapir.PublicEndpoint
import sttp.tapir.client.sttp.SttpClientInterpreter
import sttp.tapir.client.sttp.WebSocketToPipe
import sttp.capabilities.zio.ZioStreams
import zio.*
import io.stillfast.config.AppConfig
import io.stillfast.core.http.HealthRouter
import io.stillfast.projects.http.resources.CreateProjectRequest
import io.stillfast.projects.http.router.ProjectRouter
import io.stillfast.projects.model.Project
import io.stillfast.simulations.http.PerformanceTestRouter
import io.stillfast.simulations.http.resources.NewPerformanceTestRequest
import io.stillfast.simulations.model.PerformanceTest
import sttp.capabilities.WebSockets

case class TestApiClient(backend: SttpBackend[Task, Any], container: StillFastContainer) {

  private val interpreter = SttpClientInterpreter()
  private def toRequest[I, E, O, R](e: PublicEndpoint[I, E, O, R])(using
      wsToPipe: WebSocketToPipe[R],
  ): I => Request[Either[E, O], R] = {
    val baseUri = container.uri
    interpreter.toRequestThrowDecodeFailures(e, Some(baseUri))
  }

  private val getProjectEndpoint = toRequest(ProjectRouter.Endpoints.getProject)
  private val createProjectEndpoint = toRequest(ProjectRouter.Endpoints.createProject)
  private val healthEndpoint = toRequest(HealthRouter.Endpoints.health)

  private val createPerformanceTestResultEndpoint = toRequest(PerformanceTestRouter.Endpoints.createSimulation)
  private val getPerformanceTestResultEndpoint = toRequest(PerformanceTestRouter.Endpoints.getSimulation)

  def createProject(request: CreateProjectRequest) = createProjectEndpoint(request).send(backend)
  def getProject(id: Project.Id) = getProjectEndpoint(id).send(backend)

  def createPerformanceTestResult(projectId: Project.Id, request: NewPerformanceTestRequest) =
    createPerformanceTestResultEndpoint((projectId, request)).send(backend)
  def getPerformanceTestResult(projectId: Project.Id, performanceTestResultId: PerformanceTest.Id) =
    getPerformanceTestResultEndpoint((projectId, performanceTestResultId)).send(backend)

  def health() = healthEndpoint(()).send(backend)

}

object TestApiClient {

  val layer: ZLayer[StillFastContainer & SttpBackend[Task, Any], Nothing, TestApiClient] = ZLayer {
    for {
      container <- ZIO.service[StillFastContainer]
      backend <- ZIO.service[SttpBackend[Task, Any]]
    } yield TestApiClient(backend, container)
  }
}
