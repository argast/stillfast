package io.stillfast.it

import sttp.model.StatusCode
import zio.ZIO
import zio.test.assertTrue

object HealthSpec extends BaseApiSpec {

  def spec = suite("GET /health")(
    test("create a project") {
      for {
        client <- ZIO.service[TestApiClient]
        response <- client.health()
      } yield assertTrue(response.code == StatusCode.Ok, response.body == Right("OK"))
    },
  )
}
