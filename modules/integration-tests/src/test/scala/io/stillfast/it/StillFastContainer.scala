package io.stillfast.it

import com.dimafeng.testcontainers.GenericContainer
import org.testcontainers.containers.wait.strategy.Wait
import zio.*
import sttp.client3.*

class StillFastContainer {
  private val container = GenericContainer(
    dockerImage = "still-fast:latest",
    exposedPorts = Seq(8080),
    waitStrategy = Wait.forHealthcheck(),
    env = Map(
      "DB_USERNAME" -> "postgres",
      "DB_PASSWORD" -> "white_whale",
      "DB_NAME" -> "postgres",
      "PORT" -> "8080",
      "DB_HOST" -> "127.0.0.1",
    ),
  )

  def start() = ZIO.attempt(container.start())
  def stop() = ZIO.attempt(container.stop())
  def uri = uri"http://${container.host}:${container.mappedPort(8080)}"
}

object StillFastContainer {

  def make() = ZIO.attempt(new StillFastContainer)

  def layer() = ZLayer.scoped(
    ZIO.acquireRelease(
      for {
        container <- StillFastContainer.make()
        _ <- container.start()
      } yield container,
    )(_.stop().ignore),
  )
}
