package io.stillfast.it

import java.util.UUID

import sttp.model.StatusCode
import zio.*
import zio.test.*

import io.stillfast.projects.http.resources.CreateProjectRequest
import io.stillfast.projects.http.resources.ProjectResponse
import io.stillfast.projects.model.Project

object ProjectsSpec extends BaseApiSpec {

  def spec = suite("/projects")(
    suite("POST should")(
      test("create a project") {
        for {
          client <- ZIO.service[TestApiClient]
          createResponse <- client.createProject(CreateProjectRequest("test"))
          Right(body) = createResponse.body: @unchecked
          getResponse <- client.getProject(Project.Id(body.id))
        } yield assertTrue(
          createResponse.code == StatusCode.Ok,
          getResponse.code == StatusCode.Ok,
          getResponse.body == Right(body),
        )
      },
    ),
    suite("GET should")(
      test("get an existing project") {
        for {
          client <- ZIO.service[TestApiClient]
          createResponse <- client.createProject(CreateProjectRequest("test"))
          Right(body) = createResponse.body: @unchecked
          getResponse <- client.getProject(Project.Id(body.id))
        } yield assertTrue(
          getResponse.code == StatusCode.Ok,
          getResponse.body == Right(ProjectResponse(body.id, "test")),
        )
      },
      test("return an error if project doesn't exist") {
        for {
          client <- ZIO.service[TestApiClient]
          response <- client.getProject(Project.Id(UUID.randomUUID()))
        } yield assertTrue(response.code == StatusCode.NotFound)
      },
    ),
  )
}
