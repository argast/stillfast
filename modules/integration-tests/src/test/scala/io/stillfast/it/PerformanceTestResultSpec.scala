package io.stillfast.it

import java.time.LocalDateTime
import java.util.UUID

import eu.timepit.refined.auto.*
import sttp.model.StatusCode
import zio.*
import zio.test.*

import io.stillfast.projects.http.resources.CreateProjectRequest
import io.stillfast.projects.model.Project
import io.stillfast.simulations.http.resources.NewPerformanceTestRequest
import io.stillfast.simulations.model.PerformanceTest

object PerformanceTestResultSpec extends BaseApiSpec {

  def spec = suite("/projects")(
    suite("POST should")(
      test("create a performance test result for an existing project") {
        for {
          projectId <- createProject()
          client <- ZIO.service[TestApiClient]
          request = NewPerformanceTestRequest("test1", LocalDateTime.now(), LocalDateTime.now())
          performanceTestResponse <- client.createPerformanceTestResult(projectId, request)
        } yield assertTrue(
          performanceTestResponse.code == StatusCode.Ok,
          performanceTestResponse.body.map(_.name) == Right("test1"),
        )
      },
      test("fail if project doesn't exist") {
        for {
          client <- ZIO.service[TestApiClient]
          projectId = Project.Id(UUID.randomUUID())
          request = NewPerformanceTestRequest("test1", LocalDateTime.now(), LocalDateTime.now())
          performanceTestResponse <- client.createPerformanceTestResult(projectId, request)
        } yield assertTrue(
          performanceTestResponse.code == StatusCode.NotFound,
          performanceTestResponse.body.left.map(_.message) == Left(s"Project $projectId not found"),
        )
      },
    ),
    suite("GET should")(
      test("get an existing performance test result") {
        for {
          projectId <- createProject()
          client <- ZIO.service[TestApiClient]
          request = NewPerformanceTestRequest("test", LocalDateTime.now(), LocalDateTime.now())
          response <- client.createPerformanceTestResult(projectId, request)
          Right(performanceTestResult) = response.body: @unchecked
          getResponse <- client.getPerformanceTestResult(projectId, PerformanceTest.Id(performanceTestResult.id))
        } yield assertTrue(
          getResponse.code == StatusCode.Ok,
          getResponse.body == Right(performanceTestResult),
        )
      },
//      test("return an error if project doesn't exist") {
//        for {
//          client <- ZIO.service[TestApiClient]
//          response <- client.getProject(Project.Id(UUID.randomUUID()))
//        } yield assertTrue(response.code == StatusCode.NotFound)
//      },
//    ),
    ),
  )

  private def createProject(): RIO[TestApiClient, Project.Id] = for {
    client <- ZIO.service[TestApiClient]
    createResponse <- client.createProject(CreateProjectRequest("test"))
    Right(body) = createResponse.body: @unchecked
  } yield Project.Id(body.id)
}
