import com.typesafe.sbt.packager.docker.Cmd
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import GatlingPlugin.autoImport._
import TpolecatPlugin.autoImport._
import _root_.io.github.davidgregory084.DevMode

name := "still-fast"
ThisBuild / scalaVersion := Versions.scala

ThisBuild / evictionErrorLevel := Level.Warn
ThisBuild / tpolecatDefaultOptionsMode := DevMode

ThisBuild / semanticdbEnabled := true
ThisBuild / semanticdbVersion := scalafixSemanticdb.revision
ThisBuild / scalafixDependencies += "com.github.liancheng" %% "organize-imports" % Versions.scalafixOrganiseImports

//ThisBuild / libraryDependencySchemes += "org.scala-lang.modules" %% "scala-xml" % VersionScheme.Always

ThisBuild / resolvers ++= Resolver.sonatypeOssRepos("releases")

lazy val service = (project in file("modules/service"))
  .enablePlugins(JavaAppPackaging, DockerPlugin, AshScriptPlugin)
  .settings(
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic" % Versions.logback,
      "org.slf4j" % "slf4j-api" % Versions.slf4j,
      //      "com.typesafe.akka" %% "akka-stream" % Versions.akka,
      //      "com.spotify" %% "magnolify-scalacheck" % Versions.magnolify % "test",
      "com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % Versions.tapir % "test",
      "com.softwaremill.sttp.client3" %% "zio" % Versions.sttp,
      "org.scalatest" %% "scalatest" % Versions.scalaTest % "test",
      "com.lihaoyi" %% "pprint" % Versions.pprint % "test",
      "org.tpolecat" %% "doobie-core" % Versions.doobie,
      "org.tpolecat" %% "doobie-postgres" % Versions.doobie,
      "org.tpolecat" %% "doobie-hikari" % Versions.doobie,
      "org.tpolecat" %% "doobie-refined" % Versions.doobie,
      "io.scalaland" %% "chimney" % Versions.chimney,
      "io.scalaland" %% "chimney-cats" % Versions.chimney,
      "org.flywaydb" % "flyway-core" % Versions.flyway,
      //      "io.gatling.highcharts" % "gatling-charts-highcharts" % Versions.gatling % "test",
      //      "io.gatling" % "gatling-test-framework" % Versions.gatling % "test",
      //      "io.estatico" %% "newtype" % Versions.newtype,
      "com.github.pureconfig" %% "pureconfig-core" % Versions.pureconfig,
      "eu.timepit" %% "refined" % Versions.refined,
      "eu.timepit" %% "refined-scalacheck" % Versions.refined % "test",
      //      "org.scalaz" %% "scalaz-deriving" % Versions.scalazDeriving,
      //      "org.scalaz" %% "scalaz-deriving-scalacheck" % Versions.scalazDeriving % "test",
      "com.softwaremill.sttp.tapir" %% "tapir-json-zio" % Versions.tapir,
      "com.softwaremill.sttp.shared" %% "zio" % Versions.zioShared,
      "com.softwaremill.sttp.tapir" %% "tapir-vertx-server-zio" % Versions.tapir,
      "com.softwaremill.sttp.tapir" %% "tapir-zio" % Versions.tapir,
      "com.dimafeng" %% "testcontainers-scala-scalatest" % Versions.testcontainers % "test",
      "com.dimafeng" %% "testcontainers-scala-postgresql" % Versions.testcontainers % "test",
//      "io.getquill" %% "quill-jdbc-zio" % Versions.quill,
      "io.netty" % "netty-resolver-dns-native-macos" % Versions.nettyResolver classifier ("osx-x86_64"),
      "dev.zio" %% "zio" % Versions.zio,
      "dev.zio" %% "zio-managed" % Versions.zio,
      "dev.zio" %% "zio-logging-slf4j" % Versions.zioLogging,
      "dev.zio" %% "zio-json" % Versions.zioJson,
      "dev.zio" %% "izumi-reflect" % Versions.izumiReflect,
      //      "dev.zio" %% "zio-json-interop-refined" % Versions.zioJson,
      "dev.zio" %% "zio-interop-cats" % Versions.zioInteropCats,
      "dev.zio" %% "zio-test" % Versions.zioTest % "test",
      "dev.zio" %% "zio-test-sbt" % Versions.zioTest % "test",
      "dev.zio" %% "zio-test-magnolia" % Versions.zioTest % "test",
      //      "pl.iterators" %% "kebs-tagged-meta" % Versions.kebs,
      "pl.iterators" %% "kebs-opaque" % Versions.kebs,
    ),
    dockerUpdateLatest := true,
    // @formatter:off
    dockerCommands += Cmd("HEALTHCHECK", "--interval=5s", "--timeout=1s", "CMD", "curl", "-s", "http://localhost:8080/health", ">>", "/dev/null"),
    // @formatter:on
    dockerBaseImage := "ghcr.io/graalvm/jdk-community:17.0.8",
    Docker / packageName := "still-fast",
  )

lazy val `integration-tests` = (project in file("modules/integration-tests"))
  .dependsOn(service)
  .settings(
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    libraryDependencies ++= Seq(
      "com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % Versions.tapir % "test",
      "org.scalatest" %% "scalatest" % Versions.scalaTest % "test",
      "com.lihaoyi" %% "pprint" % Versions.pprint % "test",
      "com.dimafeng" %% "testcontainers-scala-scalatest" % Versions.testcontainers % "test",
      "com.dimafeng" %% "testcontainers-scala-postgresql" % Versions.testcontainers % "test",
      "dev.zio" %% "zio-test" % Versions.zioTest % "test",
      "dev.zio" %% "zio-test-sbt" % Versions.zioTest % "test",
      "dev.zio" %% "zio-test-magnolia" % Versions.zioTest % "test",
    ),
  )

lazy val `performance-tests` = (project in file("modules/performance-tests"))
  .dependsOn(service)
  .enablePlugins(GatlingPlugin)
  .settings(
    libraryDependencySchemes += "org.scala-lang.modules" %% "scala-collection-compat" % VersionScheme.Always,
    libraryDependencies ++= Seq(
      "io.gatling.highcharts" % "gatling-charts-highcharts" % Versions.gatling % "test",
      "io.gatling" % "gatling-test-framework" % Versions.gatling % "test",
    ),
  )

lazy val root = (project in file("."))
  .aggregate(service, `integration-tests`, `performance-tests`)
  .settings(
    commands += Command.command("ciBuild") { state =>
      "test" ::
        "Docker/publishLocal" ::
        "integration-tests/test" ::
        "performance-tests/test" ::
        state
    },
    resolvers ++= Resolver.sonatypeOssRepos("releases"),
  )
